package pl.krzyyy.sbhw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbHwApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbHwApplication.class, args);
    }

}
