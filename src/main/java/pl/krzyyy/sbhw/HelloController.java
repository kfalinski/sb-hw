package pl.krzyyy.sbhw;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/")
public class HelloController {

    private UUID uuid = UUID.randomUUID();

    @GetMapping
    public String s() {
        return uuid.toString();
    }

    @GetMapping("/hello")
    public String hello() {
        return "HELLO " + uuid.toString();
    }

}
